﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaWinCalculette
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void lbl_Message_Click(object sender, EventArgs e)
        {

        }

        private void cbx_Operateur_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btn_Caculer_Click(object sender, EventArgs e)
        {
            decimal nb1, nb2;
            decimal resultat = 0m;
            string operateur;
            string messageErreur = "Veuillez remplir tout les champs";
            string messageErreur2 = "Une division par 0 n'est pas possible";
            if (tbx_Nombre2.Text == "" | tbx_Nombre1.Text == "")
            {
                MessageBox.Show("Il faut saisir deux nombres et sélectionner un opérateur !",
                "Message d'erreur", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                lbl_Mess.Text =  messageErreur.ToString();
            }
            else
            {
                nb1 = Convert.ToDecimal(tbx_Nombre1.Text);
                nb2 = Convert.ToDecimal(tbx_Nombre2.Text);
                operateur = cbx_Operateur.Text;

                switch (operateur)
                { 
                    case "+":
                        resultat = nb1 + nb2;
                        break;
                    case "-":
                        resultat = nb1 - nb2;
                        break;
                    case "*":
                        resultat = nb1 * nb2;
                        break;
                    case "/":
                        if (nb2 == 0)
                        {
                            MessageBox.Show("Une division par 0 n'est pas possible",
                             "Message d'erreur", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            lbl_Mess.Text = messageErreur2.ToString();
                            tbx_Resultat.Text = "ERR !!!".ToString();
                        }
                        else
                        {
                            resultat = nb1 / nb2;     
                        }
                        break;
                }
            }
            
            tbx_Resultat.Text = resultat.ToString();

        }
    }
}
