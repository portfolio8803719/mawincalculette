﻿namespace MaWinCalculette
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbx_Nombre1 = new System.Windows.Forms.TextBox();
            this.cbx_Operateur = new System.Windows.Forms.ComboBox();
            this.tbx_Nombre2 = new System.Windows.Forms.TextBox();
            this.btn_Caculer = new System.Windows.Forms.Button();
            this.tbx_Resultat = new System.Windows.Forms.TextBox();
            this.lbl_Message = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_Mess = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbx_Nombre1
            // 
            this.tbx_Nombre1.Location = new System.Drawing.Point(154, 100);
            this.tbx_Nombre1.Name = "tbx_Nombre1";
            this.tbx_Nombre1.Size = new System.Drawing.Size(117, 22);
            this.tbx_Nombre1.TabIndex = 0;
            // 
            // cbx_Operateur
            // 
            this.cbx_Operateur.FormattingEnabled = true;
            this.cbx_Operateur.Items.AddRange(new object[] {
            "+",
            "/",
            "-",
            "*"});
            this.cbx_Operateur.Location = new System.Drawing.Point(201, 177);
            this.cbx_Operateur.Name = "cbx_Operateur";
            this.cbx_Operateur.Size = new System.Drawing.Size(70, 24);
            this.cbx_Operateur.TabIndex = 1;
            this.cbx_Operateur.SelectedIndexChanged += new System.EventHandler(this.cbx_Operateur_SelectedIndexChanged);
            // 
            // tbx_Nombre2
            // 
            this.tbx_Nombre2.Location = new System.Drawing.Point(157, 245);
            this.tbx_Nombre2.Name = "tbx_Nombre2";
            this.tbx_Nombre2.Size = new System.Drawing.Size(114, 22);
            this.tbx_Nombre2.TabIndex = 2;
            // 
            // btn_Caculer
            // 
            this.btn_Caculer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Caculer.Location = new System.Drawing.Point(310, 169);
            this.btn_Caculer.Name = "btn_Caculer";
            this.btn_Caculer.Size = new System.Drawing.Size(100, 37);
            this.btn_Caculer.TabIndex = 3;
            this.btn_Caculer.Text = "Calculer";
            this.btn_Caculer.UseVisualStyleBackColor = true;
            this.btn_Caculer.Click += new System.EventHandler(this.btn_Caculer_Click);
            // 
            // tbx_Resultat
            // 
            this.tbx_Resultat.Location = new System.Drawing.Point(462, 177);
            this.tbx_Resultat.Name = "tbx_Resultat";
            this.tbx_Resultat.Size = new System.Drawing.Size(133, 22);
            this.tbx_Resultat.TabIndex = 4;
            // 
            // lbl_Message
            // 
            this.lbl_Message.AutoSize = true;
            this.lbl_Message.Location = new System.Drawing.Point(83, 336);
            this.lbl_Message.Name = "lbl_Message";
            this.lbl_Message.Size = new System.Drawing.Size(0, 16);
            this.lbl_Message.TabIndex = 5;
            this.lbl_Message.Click += new System.EventHandler(this.lbl_Message_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 19);
            this.label1.TabIndex = 6;
            this.label1.Text = "Nombre 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(31, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 19);
            this.label2.TabIndex = 7;
            this.label2.Text = "Opérateur";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(31, 251);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 19);
            this.label3.TabIndex = 8;
            this.label3.Text = "Nombre 2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(493, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Résultat";
            // 
            // lbl_Mess
            // 
            this.lbl_Mess.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbl_Mess.Location = new System.Drawing.Point(39, 320);
            this.lbl_Mess.Name = "lbl_Mess";
            this.lbl_Mess.Size = new System.Drawing.Size(556, 73);
            this.lbl_Mess.TabIndex = 10;
            this.lbl_Mess.Text = "label5";
            // 
            // Form1
            // 
            this.AcceptButton = this.btn_Caculer;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 550);
            this.Controls.Add(this.lbl_Mess);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_Message);
            this.Controls.Add(this.tbx_Resultat);
            this.Controls.Add(this.btn_Caculer);
            this.Controls.Add(this.tbx_Nombre2);
            this.Controls.Add(this.cbx_Operateur);
            this.Controls.Add(this.tbx_Nombre1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbx_Nombre1;
        private System.Windows.Forms.ComboBox cbx_Operateur;
        private System.Windows.Forms.TextBox tbx_Nombre2;
        private System.Windows.Forms.Button btn_Caculer;
        private System.Windows.Forms.TextBox tbx_Resultat;
        private System.Windows.Forms.Label lbl_Message;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_Mess;
    }
}

